# Change Log

## [1.3.0] - 2023-08-21

-   Adding SonarLint plugin

## [1.2.0] - 2023-03-24

-   Replacing TabNine with Github Copilot

## [1.1.0] - 2021-09-29

-   Removing Bracket pair colorizer plugin

## [1.0.0] - 2021-09-29

-   Initial release
